package com.teamproject.project.services;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.NotAllowedException;
import com.teamproject.project.models.Beer;
import com.teamproject.project.models.User;
import com.teamproject.project.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceSqlImpl implements UserService {
    private final UserRepository repository;
    private final BeerService beerService;

    @Autowired
    public UserServiceSqlImpl(UserRepository repository, BeerService beerService) {
        this.repository = repository;
        this.beerService = beerService;
    }

    @Override
    public void create(User user) {
        validateUniqueEmail(user.getEmail());
        repository.create(user);
    }

    private void validateUniqueEmail(String email) {
        if (repository.existsByEmail(email)) {
            throw new DuplicateEntityException(
                    String.format("User with email %s is already registered!", email));
        }
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public List<Beer> getWishList(User user) {
        return repository.getWishList(user);
    }

    @Override
    public List<Beer> getDrunkList(User user) {
        return repository.getDrunkList(user);
    }

    @Override
    public void addToWishList(int beerId, User user) {
        Beer beerToAdd = beerService.getById(beerId);
        repository.addToWishList(beerToAdd, user);
        //TODO if beer is already in the List not to add it again
    }

    @Override
    public void addToDrunkList(int beerId, User user) {
        Beer beerToAdd = beerService.getById(beerId);
        // removeFromWishList(user, beerToAdd);
        repository.addToDrunkList(beerToAdd, user);
    } //TODO Overwrite hashcode and equals

    private void removeFromWishList(User user, Beer beer) {
        List<Beer> wishList = new ArrayList<>(getWishList(user));
        wishList.forEach(beerToRemove -> {
            if (beer.equals(beerToRemove)) {
                wishList.remove(beer);
            }
        });
    }

        @Override
        public Set<User> search (Optional < Integer > id, Optional < String > name, Optional < String > email){
            return null;
            //TODO Kalina
        }

        @Override
        public void update ( int id, User user, User updater){
            if (updater.getId() != id && updater.getRoles().stream().noneMatch(r -> r.getName().equals("Admin"))) {
                throw new NotAllowedException(
                        String.format("User %s is not allowed to edit the given profile", updater.getUsername()));
            }
            repository.update(id, user);
//        updater.getId() != id && updater.getRoles().stream().noneMatch(r -> r.getName().equals("Admin")
            //TODO why it does not work
        }

        @Override
        public void delete ( User user, String username){
        User deleter = getByUsername(username);
            if (user.getUsername().equals("username")
                    || deleter.getRoles().stream().anyMatch(role -> role.getName().equals("Admin"))) {
                throw new NotAllowedException(
                        String.format("User %s is not allowed to delete the given profile", deleter.getUsername()));
            }
            repository.delete(user.getId());
            }
//TODO Kalina
        }


