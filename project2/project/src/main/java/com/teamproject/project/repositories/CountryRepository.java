package com.teamproject.project.repositories;

import com.teamproject.project.models.Country;

import java.util.List;
import java.util.Optional;

public interface CountryRepository {

    List<Country> getAllCountries();

    Country getById(int id);

    List<Country> search(Optional<Integer> id, Optional<String> name);

    void create(Country country);

    void update(Country country, int id);

    void delete(int id);

    boolean existsByName(String name);

}
