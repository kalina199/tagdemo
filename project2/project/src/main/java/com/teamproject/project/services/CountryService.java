package com.teamproject.project.services;

import com.teamproject.project.models.Country;

import java.util.List;

public interface CountryService {

    void create (Country country);

    Country getById (int id);

    List<Country> getAll();

    void update (Country country, int id);

    void delete (int id);
}
