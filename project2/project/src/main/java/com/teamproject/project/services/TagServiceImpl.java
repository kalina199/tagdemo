package com.teamproject.project.services;

import com.teamproject.project.models.Tag;
import com.teamproject.project.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tag> getAllTags() {
        return repository.getAllTags();
    }

    @Override
    public Tag getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void create(Tag tag) {
        // validation by name
        repository.create(tag);
    }

    @Override
    public void update(Tag tag) {
        // validation by name
        repository.update(tag);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public List<Tag> filter(Optional<String> name) {
        return repository.filter(name);
    }
}
