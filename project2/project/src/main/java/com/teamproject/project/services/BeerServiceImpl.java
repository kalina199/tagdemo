package com.teamproject.project.services;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.models.Beer;
import com.teamproject.project.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
    private final BeerRepository repository;

    @Autowired
    public BeerServiceImpl(BeerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Beer> getAllBeers() {
        return repository.getAllBeers();
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Beer> filterByName(String name) {
        return repository.filterByName(name);
    }

    @Override
    public List<Beer> filterByStyle(Integer styleId) {
        return repository.filterByStyle(styleId);
    }

    @Override
    public List<Beer> filterByCountry(Integer countryId) {
        return repository.filterByCountry(countryId);
    }

    @Override
    public List<Beer> filterByTag(String tag) {
        return repository.filterByTag(tag);
    }

    @Override
    public void create(Beer beer, String username) {
        validateUniqueBeerName(beer.getName());
        if (!username.equalsIgnoreCase(beer.getOwner()) && !username.equalsIgnoreCase("Admin")) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit the given profile", username));
        }
        repository.create(beer);
    }

    @Override
    public void update(Beer beer, String username) {
        validateUniqueBeerId(beer.getName(), beer.getId());
        if (!username.equalsIgnoreCase(beer.getOwner()) && !username.equalsIgnoreCase("Admin")) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit the given profile", username));
        }
        repository.update(beer);
    }

    @Override
    public void delete(int id, String username) {
        if (!username.equalsIgnoreCase(getById(id).getOwner()) && !username.equalsIgnoreCase("Admin")) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit the given profile", username));
        }
        repository.delete(id);
    }

    private void validateUniqueBeerName(String name) {
        if (repository.existsByName(name)) {
            throw new DuplicateEntityException(
                    String.format("Beer with name %s already exists!", name)
            );
        }
    }

    private void validateUniqueBeerId(String name, int id) {
        if (repository.existsByName(name) && (repository.getById(id).getId() != id)) {
            throw new DuplicateEntityException(
                    String.format("Beer with name %s already exists!", name)
            );
        }
    }

    public boolean existsByName(String name) {
        return repository.existsByName(name);
    }

    @Override
    public List<Beer> sortByAbv() {
        return repository.sortByABV();
    }

    @Override
    public List<Beer> sortByName() {
        return repository.sortByName();
    }

//    @Override
//    public double avgRating(int beerId) {
//        return repository.avgRating(beerId);
//    }
}
