package com.teamproject.project.repositories;

import com.teamproject.project.models.Beer;

import java.util.List;

public interface BeerRepository {

    List<Beer> getAllBeers();

    Beer getById(int id);

//    List<Beer> search(String name);

    List<Beer> filterByName(String name);

    List<Beer> filterByStyle(Integer styleId);

    List<Beer> filterByCountry(Integer countryId);

    List<Beer> filterByTag(String tag);

    List<Beer> sortByABV();

    List<Beer> sortByName();

//   double avgRating(int beerId);

    void create(Beer beer);

    void update(Beer beer);

    void delete(int id);



    boolean existsByName(String name);

//    Filter by country, style, tags
//    ii. Sort by name, ABV, rating
}
