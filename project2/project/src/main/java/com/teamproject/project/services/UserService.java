package com.teamproject.project.services;

import com.teamproject.project.models.Beer;
import com.teamproject.project.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {
    void create (User user);

    User getById(int id);

    User getByUsername(String userName);

    List<User> getAll();

    List<Beer> getWishList(User user);

    Set<User> search(Optional<Integer> id, Optional<String> name, Optional<String> email);

    void update (int id, User user, User updater);

    void delete (User user, String username);

    List<Beer> getDrunkList(User user);

    void addToWishList(int beerId, User user);

    void addToDrunkList(int beerId, User user);
}
