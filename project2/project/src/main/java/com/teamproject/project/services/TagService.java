package com.teamproject.project.services;

import com.teamproject.project.models.Tag;

import java.util.List;
import java.util.Optional;

public interface TagService {

    List<Tag> getAllTags();

    Tag getById(int id);

    void create (Tag tag);

    void update(Tag tag);

    void delete (int id);

    List<Tag> filter(Optional<String> name);
}
