package com.teamproject.project.models.dto;

public class DisplayRatingDTO {

    private double rating;
    private String beer;
    private String user;

    public DisplayRatingDTO(double rating, String beer, String user) {
        this.rating = rating;
        this.beer = beer;
        this.user = user;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getBeer() {
        return beer;
    }

    public void setBeer(String beer) {
        this.beer = beer;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
