package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Style;
import com.teamproject.project.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {

    private final StyleService service;

    @Autowired
    public StyleRestController(StyleService service) {
        this.service = service;
    }

    @GetMapping
    public List<Style> getAllStyles() {
        return service.getAllStyles();
    }

    @GetMapping("/{id}")
    public Style getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public Style create(@Valid @RequestBody Style style) {
        try {
            service.create(style);
            return style;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Style update(@Valid @RequestBody Style style) {
        try {
            service.update(style);
            return style;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
