package com.teamproject.project.exceptions;

public class NotAllowedException extends RuntimeException{
    public NotAllowedException (String message) {
        super(message);
    }
}
