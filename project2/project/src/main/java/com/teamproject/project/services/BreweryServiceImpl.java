package com.teamproject.project.services;

import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.models.Brewery;
import com.teamproject.project.models.User;
import com.teamproject.project.repositories.BreweryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {
    private final BreweryRepository repository;

    public BreweryServiceImpl(BreweryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Brewery brewery) {
        repository.create(brewery);
    }

    @Override
    public Brewery getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Brewery> getAll() {
        return repository.getAllBreweries();
    }

//    @Override
//    public void update(Brewery brewery) {
//
//    }

    @Override
    public void update(int breweryId, Brewery brewery, User updater) {
        //TODO
        if (updater.getRoles().stream().anyMatch(r->r.equals("Admin"))) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit the given brewery", updater.getUsername()));
        }
        repository.update(brewery, breweryId);
    }

    @Override
    public void delete(int id, User user) {
        //TODO
        if (!user.getRoles().stream().anyMatch(r->r.equals("Admin"))) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit the given brewery", user.getUsername()));
        }
        repository.delete(id);
    }
}
