package com.teamproject.project.repositories;

import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Brewery;
import com.teamproject.project.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
    private final SessionFactory sessionFactory;

    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAllCountries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country ", Country.class);
            return query.list();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new EntityNotFoundException(
                        String.format("Country with id %d not found.", id));
            }
            return country;
        }
    }

    @Override
    public List<Country> search(Optional<Integer> id, Optional<String> name) {
        return null;
    }

    @Override
    public void create(Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.save(country);
        }
    }

    @Override
    public void update(Country country, int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Country countryToUpdate = session.get(Country.class, id);
            session.update(countryToUpdate);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Country countryToDelete = getById(id);


        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            countryToDelete.setDeleted(true);
            session.update(countryToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean existsByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery(
                    "from Country where name = :name order by name", Country.class
            );
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
}
