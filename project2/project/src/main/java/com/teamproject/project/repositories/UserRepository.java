package com.teamproject.project.repositories;

import com.teamproject.project.models.Beer;
import com.teamproject.project.models.User;
import com.teamproject.project.models.dto.DisplayBeerDTO;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository {
    void create(User user);

    User getById(int id);

    User getByUsername(String username);

    Set<User> search(Optional<Integer> id, Optional<String> name, Optional<String> email);

    boolean existsByEmail (String email);

    List<User> getAll();

    void update(int id, User user);

    void delete (int id);

    List<Beer> getWishList(User user);

    List<Beer> getDrunkList(User user);

    void addToWishList(Beer beer, User user);

    void addToDrunkList(Beer beer, User user);
}
