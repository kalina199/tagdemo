package com.teamproject.project.repositories;

import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public class BreweryRepositorySqlImpl implements BreweryRepository {
    private final SessionFactory sessionFactory;

    public BreweryRepositorySqlImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAllBreweries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery ", Brewery.class);
            return query.list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new EntityNotFoundException(
                        String.format("Brewery with id %d not found.", id));
            }
            return brewery;
        }
    }

    @Override
    public List<Brewery> search(Optional<Integer> id, Optional<String> name) {
        return null;
    }
//TODO if necessary

    @Override
    public void create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
    }



   // @Override //TODO Kalina
    public void update(Brewery brewery, int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Brewery breweryToUpdate = session.get(Brewery.class, id);
            session.update(breweryToUpdate);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Brewery breweryToDelete = getById(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            breweryToDelete.setDeleted(true);
            session.update(breweryToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean existsByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery(
                    "from Brewery where name = :name order by name", Brewery.class
            );
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
}
