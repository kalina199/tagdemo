package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.exceptions.NotAllowedException;
import com.teamproject.project.models.Beer;
import com.teamproject.project.models.User;
import com.teamproject.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/users")
public class UserRestController<BeerDto> {

    private final UserService service;

    @Autowired
    public UserRestController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public List<User> getAllUsers() {
        return service.getAll();
    }


    @GetMapping("/id/{id}")
    public User getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{username}")
    //TODO Kalina to remove from this layer if not used
    public User getByUsername(@PathVariable String username) {
        try {
            return service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Set<User> search(@RequestParam(required = false) Integer id,
                            @RequestParam(required = false) String name,
                            @RequestParam(required = false) String email) {

        return service.search(Optional.ofNullable(id), Optional.ofNullable(name), Optional.ofNullable(email));
    }

    @PostMapping
    public User create(@Valid @RequestBody User user) {
        try {
            service.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @Valid @RequestBody User user, @RequestHeader("Authorization") String username) {
        try {
            User updater = service.getByUsername(username);
            service.update(id, user, updater);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @DeleteMapping
    public void delete(@Valid @RequestBody User user, @RequestHeader("Authorization") String username) {
        try {
            service.delete(user, username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (NotAllowedException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/wishList/{id}")
    public List<Beer> getWishList(@PathVariable int id) {
        try {
            User user = service.getById(id);
            return service.getWishList(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/drunkList/{id}")
    public List<Beer> getDrunkList(@PathVariable int id) {
        try {
            User user = service.getById(id);
            return service.getDrunkList(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/beerToWishList/{beerId}")
    public void addToWishList(@PathVariable int beerId, @RequestHeader("Authorization") String username) {
        try {
            User user = service.getByUsername(username);
            service.addToWishList(beerId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/beerToDrunkList/{beerId}")
    public void addToDrunkList(@PathVariable int beerId, @RequestHeader("Authorization") String username) {
        try {
            User user = service.getByUsername(username);
            service.addToDrunkList(beerId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
