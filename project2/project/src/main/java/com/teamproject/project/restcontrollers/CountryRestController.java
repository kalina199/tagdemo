package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Country;
import com.teamproject.project.models.User;
import com.teamproject.project.services.CountryService;
import com.teamproject.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {
    private final CountryService service;
    private final UserService userService;

    @Autowired
    public CountryRestController(CountryService service, UserService userService) {
        this.service = service;
        this.userService=userService;
    }

    @GetMapping
    public List<Country> getAllCountries() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Country create(@Valid @RequestBody Country country) {
        try {
            service.create(country);
            return country;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Country update(@Valid @RequestBody Country country, @RequestHeader("Authorization") String username) {
        try {
            User updater = userService.getByUsername(username);
            int id = updater.getId();
            service.update(country, id);
            return country;
        }
        catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        try {
            service.delete(id);
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
