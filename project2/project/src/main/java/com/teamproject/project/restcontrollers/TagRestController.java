package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Tag;
import com.teamproject.project.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {

    private final TagService service;

    @Autowired
    public TagRestController(TagService service) {
        this.service = service;
    }

    @GetMapping
    public List<Tag> getAllTags() {
        return service.getAllTags();
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/filter")
    public List<Tag> filter(@RequestParam(required = false) String name) {
        return service.filter(Optional.ofNullable(name));
    }

    @PostMapping
    public Tag create(@Valid @RequestBody Tag tag) {
        try {
            service.create(tag);
            return tag;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Tag update(@Valid @RequestBody Tag tag) {
        try {
            service.update(tag);
            return tag;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}