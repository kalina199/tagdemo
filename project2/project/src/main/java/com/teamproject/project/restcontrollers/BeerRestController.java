package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.models.Beer;
import com.teamproject.project.models.dto.DisplayBeerDTO;
import com.teamproject.project.models.dto.DisplayStyleDTO;
import com.teamproject.project.models.dto.DisplayTagDTO;
import com.teamproject.project.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

    private final BeerService service;

    @Autowired
    public BeerRestController(BeerService service) {
        this.service = service;
    }

    @GetMapping
    public List<Beer> getAll() {
        return service.getAllBeers();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filterByName")
    public List<Beer> filterByName(@RequestParam(required = false) String name) {
        return service.filterByName(name);
    }

    @GetMapping("/filterByStyle")
    public List<Beer> filterByStyleId(@RequestParam(required = false) Integer styleId) {
        return service.filterByStyle(styleId);
    }

    @GetMapping("/filterByCountry")
    public List<DisplayBeerDTO> filterByCountry(@RequestParam Integer countryId) {
        return service.filterByCountry(countryId)
                .stream()
                .map(beer -> new DisplayBeerDTO
                        (beer.getName(),
                                beer.getAbv(),
                                beer.getBrewery().getName(),
                                beer.getCountry().getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/filterByTag")
    public List<Beer> filterByTagId(@RequestParam(required = false) String tag) {
        return service.filterByTag(tag);
    }

    @PostMapping
    public Beer create(@Valid @RequestBody Beer beer, String username) {
        try {
            service.create(beer, username);
            return beer;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping
    public Beer update(@Valid @RequestBody Beer beer, @RequestHeader("Authorization") String username) {
        try {
            service.update(beer, username);
            return beer;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader("Authorization")  String username) {
        try {
            service.delete(id, username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/displayInfo")
    public List<DisplayBeerDTO> getBeerDisplayInfo() {
        return service.getAllBeers()
                .stream()
                .map(beer -> new DisplayBeerDTO
                        (beer.getName(), beer.getAbv(), beer.getBrewery().getName(), beer.getCountry().getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/displayStyle")
    public List<DisplayStyleDTO> getBeerStyleInfo() {
        return service.getAllBeers()
                .stream()
                .map(beer -> new DisplayStyleDTO
                        (beer.getName(), beer.getStyle().getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/displayTag")
    public List<DisplayTagDTO> getBeerTagInfo() {
        return service.getAllBeers()
                .stream()
                .map(beer -> new DisplayTagDTO
                        (beer.getName(), beer.getTag().toString()))
                .collect(Collectors.toList());
    }

    @GetMapping("/sortByABV")
    public List<DisplayBeerDTO> sortByABVDTO () {
        return service.sortByAbv()
                .stream()
                .map(beer ->new DisplayBeerDTO
                        (beer.getName(), beer.getAbv(), beer.getBrewery().getName(), beer.getCountry().getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/sortByName")
    public List<Beer> sortByName () {
        return service.sortByName();
    }




//    @GetMapping("/search")
//    public List<Beer> search(@RequestParam(required = false) Integer id,
//                             @RequestParam(required = false) String name) {
//
//        return service.search(Optional.ofNullable(id), Optional.ofNullable(name));
//    }
}
