package com.teamproject.project.repositories;

import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class StyleRepositoryImpl implements StyleRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAllStyles() {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style ", Style.class);
            return query.list();
        }
    }

    @Override
    public Style getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException(
                        String.format("Style with id %d not found!", id));
            }
            return style;
        }
    }

    @Override
    public void create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
    }

    @Override
    public void update(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Style styleToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            styleToDelete.setDeleted(true);
            session.update(styleToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean existsByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery(
                    "from Style where name = :name order by name", Style.class
            );
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
}
