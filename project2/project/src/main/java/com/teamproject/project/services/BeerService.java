package com.teamproject.project.services;

import com.teamproject.project.models.Beer;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

public interface BeerService {

    List<Beer> getAllBeers();

    Beer getById(int id);

//    List<Beer> search(Optional<Integer> id, Optional<String> name);

    List<Beer> filterByName(String name);

    List<Beer> filterByStyle(Integer styleId);

    List<Beer> filterByCountry (Integer countryId);

    List<Beer> filterByTag(String tag);

//    double avgRating(int beerId);

    void create(Beer beer, @RequestHeader("Authorization") String username);

    void update(Beer beer, @RequestHeader("Authorization") String username);

    void delete(int id, @RequestHeader("Authorization") String username);

    boolean existsByName(String name);

    List<Beer> sortByAbv();

    List<Beer> sortByName();
}
