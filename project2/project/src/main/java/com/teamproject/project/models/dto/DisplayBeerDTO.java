package com.teamproject.project.models.dto;

public class DisplayBeerDTO {
    private String name;
    private double ABV;
    private String brewery;
    private String country;
//    private double userRating;
//    private double avgRating;

    public DisplayBeerDTO() {
    }

    public DisplayBeerDTO(String name, double ABV, String brewery, String country/* double userRating, double avgRating*/) {
        setName(name);
        setABV(ABV);
        setBrewery(brewery);
        setCountry(country);
//        setAvgRating(avgRating);
//        setUserRating(userRating);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getABV() {
        return ABV;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

//    public double getUserRating() {
//        return userRating;
//    }
//
//    public void setUserRating(double userRating) {
//        this.userRating = userRating;
//    }

//    public double getAvgRating() {
//        return avgRating;
//    }
//
//    public void setAvgRating(double avgRating) {
//        this.avgRating = avgRating;
//    }
}
