package com.teamproject.project.services;

import com.teamproject.project.models.Brewery;
import com.teamproject.project.models.User;

import java.util.List;

public interface BreweryService {

    void create(Brewery brewery);

    Brewery getById(int id);

    List<Brewery> getAll();

    void update(int breweryId, Brewery brewery, User updater);

    void delete(int id, User whoDeletes);
}
