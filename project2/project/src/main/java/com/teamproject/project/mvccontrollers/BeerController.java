package com.teamproject.project.mvccontrollers;

import com.teamproject.project.models.Beer;
import com.teamproject.project.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping
public class BeerController {

    private final BeerService beerService;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping("/beers")
    public String showBeers(Model model){
        List<Beer> beers = beerService.getAllBeers();

        model.addAttribute("beers", beers);
        return "results";
    }
}
