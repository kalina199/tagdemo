package com.teamproject.project.repositories;

import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Beer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Transactional
public class BeerRepositorySqlImpl implements BeerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BeerRepositorySqlImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAllBeers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer", Beer.class);
            return query.list();
        }
    }

    @Override
    public Beer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
           Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException(
                        String.format("Beer with id %d not found!", id));
            }
            return beer;
        }
    }

    @Override
    public List<Beer> filterByName(String name) {
        if (name == null) {
            return getAllBeers();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer where name like concat('%',:name,'%') order by name", Beer.class
            );
            query.setParameter("name", name);
            return query.list();
        }
    }

    @Override
    public List<Beer> filterByStyle(Integer styleId) {
        if (styleId == null) {
            return getAllBeers();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer where style.id = :styleId order by style.id ", Beer.class
            );
            query.setParameter("styleId", styleId);
            return query.list();
        }
    }

    @Override
    public List<Beer> filterByCountry(Integer countryId) {
        if (countryId == null) {
            return getAllBeers();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer where country.id = :countryId order by country.id ", Beer.class
            );
            query.setParameter("countryId", countryId);
            return query.list();
        }
    }

    @Override
    public List<Beer> filterByTag(String tag) {
        if (tag == null) {
            return getAllBeers();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer join Tag where Tag.name = :tag", Beer.class
            );
            query.setParameter("tag", tag);
            return query.list();
        }
    }

    @Override
    public List<Beer> sortByABV() {
        List<Beer> sortedByABV = getAllBeers().stream()
                .sorted(Comparator.comparing(Beer::getAbv))
                .collect(Collectors.toList());

        return  sortedByABV;
    }

    @Override
    public List<Beer> sortByName() {
        List<Beer> sortedByName = getAllBeers().stream()
                .sorted(Comparator.comparing(Beer::getName))
                .collect(Collectors.toList());

        return  sortedByName;
    }

    @Override
    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public void update(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Beer beerToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            beerToDelete.setDeleted(true);
            session.update(beerToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean existsByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer where name = :name order by name", Beer.class
            );
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    public double avgRating(int beerId){
        try (Session session = sessionFactory.openSession()){
            Query<Object> query = session.createSQLQuery("select AVG(data_beer_tag.ratings.rating)" +
                    " from data_beer_tag.ratings where data_beer_tag.ratings.beer_id= :beerId");

            query.setParameter("beerId", beerId);

            return (double) query.getSingleResult();
        }
    }
}
