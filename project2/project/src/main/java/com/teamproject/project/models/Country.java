package com.teamproject.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "origin_countries")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 2, max = 20, message = "Name should be between 2 & 20 symbols")
    @Column(name = "country")
    private String name;

    @JsonIgnore
    @Column(name = "is_deleted")
    private boolean deleted;

    public Country(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Country(int id, @Size(min = 2, max = 20, message = "Name should be between 2 & 20 symbols") String name, boolean deleted) {
        this.id = id;
        this.name = name;
        this.deleted = deleted;
    }

    public Country() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
