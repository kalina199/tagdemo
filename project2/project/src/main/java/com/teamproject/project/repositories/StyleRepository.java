package com.teamproject.project.repositories;

import com.teamproject.project.models.Style;

import java.util.List;

public interface StyleRepository {

    List<Style> getAllStyles();

    Style getById(int id);

    void create (Style style);

    void update(Style style);

    void delete (int id);

    boolean existsByName(String name);
}
