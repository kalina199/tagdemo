package com.teamproject.project.services;

import com.teamproject.project.models.Rating;

import java.util.List;

public interface RatingService {

    double avgRating(int beerId);

    void create(Rating rating);

    void update(Rating rating);

    List<Rating> getAll();
}
