package com.teamproject.project.repositories;

import com.teamproject.project.models.Rating;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private final SessionFactory sessionFactory;

    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Rating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Rating> query = session.createQuery("from Rating ", Rating.class);
            return query.list();
        }
    }

    @Override
    public double avgRating(int beerId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Object> query = session.createSQLQuery("select AVG(data_beer_tag.ratings.rating)" +
                    " from data_beer_tag.ratings where data_beer_tag.ratings.beer_id= :beerId");

            query.setParameter("beerId", beerId);

            return (double) query.getSingleResult();
        }
    }

    @Override
    public void create(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rating);
        }
    }

    @Override
    public void update(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rating);
            session.getTransaction().commit();
        }
    }


    public boolean existsByUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Object> query = session.createSQLQuery("select data_beer_tag.ratings.beer_id" +
                    " from data_beer_tag.ratings where data_beer_tag.ratings.user_id= :userId");
            query.setParameter("userId", userId);
            return !query.list().isEmpty();
        }
    }
}
