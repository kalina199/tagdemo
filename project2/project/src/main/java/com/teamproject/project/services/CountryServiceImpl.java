package com.teamproject.project.services;

import com.teamproject.project.models.Country;
import com.teamproject.project.repositories.CountryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private final CountryRepository repository;

    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public void create(Country country) {
        repository.create(country);
    }

    @Override
    public Country getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Country> getAll() {
        return repository.getAllCountries();
    }

    @Override
    public void update(Country country, int id) {
        repository.update(country, id);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }
}
