package com.teamproject.project.services;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.models.Style;
import com.teamproject.project.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {

    private final StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAllStyles() {
        return styleRepository.getAllStyles();
    }

    @Override
    public Style getById(int id) {
        return styleRepository.getById(id);
    }

    @Override
    public void create(Style style) {
        validateUniqueStyleName(style.getName());
//        if (!username.equalsIgnoreCase(beer.getOwner()) && !username.equalsIgnoreCase("Admin")) {
//            throw new InvalidOperationException(
//                    String.format("User %s is not allowed to edit the given profile", username));
//        }
        styleRepository.create(style);
    }

    @Override
    public void update(Style style) {
        validateUniqueStyleName(style.getName());
        styleRepository.update(style);
    }

    @Override
    public void delete(int id) {
        styleRepository.delete(id);
    }

    private void validateUniqueStyleName(String name) {
        if (styleRepository.existsByName(name)) {
            throw new DuplicateEntityException(
                    String.format("Style with name %s already exists!", name)
            );
        }
    }

    public boolean existsByName(String name) {
        return styleRepository.existsByName(name);
    }


}
