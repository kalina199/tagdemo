package com.teamproject.project.repositories;

import com.teamproject.project.models.Brewery;

import java.util.List;
import java.util.Optional;

public interface BreweryRepository {

    List<Brewery> getAllBreweries();

    Brewery getById(int id);

    List<Brewery> search(Optional<Integer> id, Optional<String> name);

    void create (Brewery brewery);

    void update(Brewery brewery, int id);

    void delete (int id);

    boolean existsByName(String name);
}
