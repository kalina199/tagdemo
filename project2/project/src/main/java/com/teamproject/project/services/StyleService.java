package com.teamproject.project.services;

import com.teamproject.project.models.Style;

import java.util.List;
import java.util.Optional;

public interface StyleService {

    List<Style> getAllStyles();

    Style getById(int id);

    void create (Style style);

    void update(Style style);

    void delete (int id);

}
