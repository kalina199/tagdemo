package com.teamproject.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="username")
    @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols")
    private String username;

    @Column(name="email")
    @Size(min = 4, max = 40, message = "Email should be between 4 and 40 symbols")
    @Email
    private String email;

    //@JsonIgnore
    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name="users_id"),
            inverseJoinColumns = @JoinColumn(name = "roles_id")
    )
    private Set<Role> roles;

    @JsonIgnore
    @Column(name = "is_deleted")
    private boolean isDeleted;

    @JsonIgnore
    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable (
            name = "wish_lists",
            joinColumns = @JoinColumn (name = "user_id" ),
            inverseJoinColumns = @JoinColumn (name = "beer_id")
    )
    private Set<Beer> beersWishList;

    @JsonIgnore
    @ManyToMany (fetch = FetchType.EAGER)
    @JoinTable (
            name = "drunk_lists",
            joinColumns = @JoinColumn (name = "user_id" ),
            inverseJoinColumns = @JoinColumn (name = "beer_id")
    )
    private Set<Beer> beersDrunkList;

    public User() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public Set<Beer> getBeersWishList() {
        return beersWishList;
    }

    public void setBeersWishList(Set<Beer> beersWishList) {
        this.beersWishList = beersWishList;
    }


    public Set<Beer> getBeersDrunkList() {
        return beersDrunkList;
    }

    public void setBeersDrunkList(Set<Beer> beersDrunkList) {
        this.beersDrunkList = beersDrunkList;
    }
}
