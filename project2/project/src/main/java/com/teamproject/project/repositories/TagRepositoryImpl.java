package com.teamproject.project.repositories;

import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Tag;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class TagRepositoryImpl implements TagRepository{

    private List<Tag> tags = new ArrayList<>();
    private int lastId = 4;

    public TagRepositoryImpl() {
        tags.add(new Tag(1, "Good"));
        tags.add(new Tag(2, "OK"));
        tags.add(new Tag(3, "Super"));
    }

    @Override
    public List<Tag> getAllTags() {
        return tags;
    }

    @Override
    public Tag getById(int id) {
        return tags.stream().filter(tag -> tag.getId() == id).findFirst()
                .orElseThrow(() -> new EntityNotFoundException(
                String.format("Tag with id %d not found!", id)));
    }

    @Override
    public void create(Tag tag) {
        tag.setId(lastId++);
        tags.add(tag);
    }

    @Override
    public void update(Tag tag) {
        Tag tagToUpdate = getById(tag.getId());
        tagToUpdate.setName(tag.getName());
    }

    @Override
    public void delete(int id) {
        Tag tagToDelete = getById(id);
        tags.remove(tagToDelete);
    }

    @Override
    public List<Tag> filter(Optional<String> name) {
        List<Tag> result = new ArrayList<>();

        if (name.isPresent()) {
            result.addAll(tags
                    .stream()
                    .filter(t -> t.getName().equalsIgnoreCase(name.get()))
                    .collect(Collectors.toList()));
        }
        return new ArrayList<>(result);
    }
}
