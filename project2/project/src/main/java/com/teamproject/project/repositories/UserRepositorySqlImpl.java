package com.teamproject.project.repositories;

import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.models.Beer;
import com.teamproject.project.models.Role;
import com.teamproject.project.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UserRepositorySqlImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositorySqlImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            Role role = new Role();
            role.setName("regular");
            user.getRoles().add(role);
            session.save(user);
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);

            List<User> users = query.list().stream().filter(user -> !user.isDeleted()).collect(Collectors.toList());
            return users;
        }
        // TODO to check if I should remove deleted users in this layer
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if ((user == null)|| user.isDeleted()) {
                throw new EntityNotFoundException(
                        String.format("User with id %d not found.", id));
            }
            return user;
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("User with username %s not found.", username));
            }
            return users.get(0);
        }
    }

    @Override
    public void update(int id, User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            //     User userToUpdate = session.get(User.class, id);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            userToDelete.setDeleted(true);
            session.update(userToDelete);
            session.getTransaction().commit();
        }
    }


    @Override
    public Set<User> search(Optional<Integer> id, Optional<String> name, Optional<String> email) {
        return null;
        //TODO Kalina
    }

    @Override
    public boolean existsByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> users = query.list();
            return !users.isEmpty();
        }
        //TODO Kalina to check if it works properly
    }


    @Override
    public List<Beer> getWishList(User user) {
        int userId = user.getId();
        try (Session session = sessionFactory.openSession()) {

            String sql = "select * from beers b join wish_lists wl on b.id = wl.beer_id where wl.user_id=:userId";
            NativeQuery<Beer> query = session.createNativeQuery(sql, Beer.class);
            query.addEntity("beers", Beer.class);
            query.setParameter("userId", userId);
            return query.list();
//            Set<Beer> wishList = new HashSet<>();
//            List<Beer> beers = query.list();
//            for (Beer beer:beers) {
//                wishList.add(beer);
//            }
//            return wishList;
            //TODO check if there is cleaner way to fill the set with the query results
            //TODO why it does not work with Set<Beer>
        }
    }

    @Override
    public List<Beer> getDrunkList(User user) {
        int userId = user.getId();
        try (Session session = sessionFactory.openSession()) {

            String sql = "select * from beers b join drunk_lists wl on b.id = wl.beer_id where wl.user_id=:userId";
            NativeQuery<Beer> query = session.createNativeQuery(sql, Beer.class);
            // query.addEntity("beers", Beer.class);
            query.setParameter("userId", userId);
            return query.list();
        }
    }

    @Override
    public void addToWishList(Beer beer, User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.getBeersWishList().add(beer);
            session.update(user);
            session.getTransaction().commit();
        }
        //TODO why it shows the beers twice
    }

    @Override
    public void addToDrunkList(Beer beer, User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.getBeersDrunkList().add(beer);
            session.update(user);
            session.getTransaction().commit();
        }
        //TODO to remove the beer from wishList if it is there
        //TODO why it shows the beers twice
    }



}

