package com.teamproject.project.models.dto;

public class DisplayTagDTO {

    private String beerName;
    private String tagName;

    public DisplayTagDTO(String beerName, String tagName) {
        this.beerName = beerName;
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }
}
