package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.DuplicateEntityException;
import com.teamproject.project.exceptions.EntityNotFoundException;
import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.models.Beer;
import com.teamproject.project.models.Brewery;
import com.teamproject.project.models.User;
import com.teamproject.project.services.BeerService;
import com.teamproject.project.services.BreweryService;
import com.teamproject.project.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {
    private final BreweryService service;
    private final UserService userService;
    private final BeerService beerService;

    public BreweryRestController(BreweryService service, UserService userService, BeerService beerService) {
        this.service = service;
        this.userService = userService;
        this.beerService = beerService;
    }

    @GetMapping
    public List<Brewery> getAllBreweries() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Brewery getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery create(@Valid @RequestBody Brewery brewery) {
        try {
            service.create(brewery);
            return brewery;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}/beers")
    public List<Beer> getStyleBeers (@PathVariable int id){
        return beerService.filterByStyle(id);
    }

    @PutMapping("/{id}")
    public Brewery update(@PathVariable int id, @Valid @RequestBody Brewery brewery, @RequestHeader("Authorization") String username) {
        try {
            User updater = userService.getByUsername(username);
            service.update(id, brewery, updater);
            return brewery;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader("Authorization") String username) {
        try {
            User user = userService.getByUsername(username);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
