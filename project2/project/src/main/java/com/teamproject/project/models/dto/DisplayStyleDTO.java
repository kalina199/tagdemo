package com.teamproject.project.models.dto;

public class DisplayStyleDTO {

    private String beerName;
    private String styleName;


    public DisplayStyleDTO(String beerName, String styleName) {
        this.beerName = beerName;
        this.styleName = styleName;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }
}
