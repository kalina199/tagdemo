package com.teamproject.project.restcontrollers;

import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.models.Rating;
import com.teamproject.project.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/rating")
public class RatingRestController {

    private final RatingService ratingService;

    @Autowired
    public RatingRestController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<Rating> getAllRatings() {
        return ratingService.getAll();
    }

    @GetMapping ("/{beerId}")
    double avgRating(@PathVariable int beerId) {
        return ratingService.avgRating(beerId);
    }

    @PostMapping
    public Rating create(@Valid @RequestBody Rating rating)  {
        try {
            ratingService.create(rating);
            return rating;
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping
    public Rating update(@Valid @RequestBody Rating rating) {
        try {
            ratingService.update(rating);
            return rating;
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
