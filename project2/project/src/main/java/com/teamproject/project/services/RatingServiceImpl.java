package com.teamproject.project.services;

import com.teamproject.project.exceptions.InvalidOperationException;
import com.teamproject.project.models.Rating;
import com.teamproject.project.repositories.RatingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;

    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }



    @Override
    public double avgRating(int beerId) {
        return ratingRepository.avgRating(beerId);
    }

    @Override
    public void create(Rating rating) {
        if (existsByUser(rating.getUser().getId())) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", rating.getUser().getUsername()));
        }
       ratingRepository.create(rating);
    }

    @Override
    public void update(Rating rating) {
        if (!existsByUser(rating.getUser().getId()))
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", rating.getUser().getUsername()));

        ratingRepository.update(rating);
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepository.getAll();
    }

    public boolean existsByUser(int userId) {
        return ratingRepository.existsByUser(userId);
    }

}
