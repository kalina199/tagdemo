# **Beer Tag**

*Java Team Project*

Trello board - https://trello.com/b/SXtef6IE/beertag


## Project Description
Your task is to develop BEER TAG web application. BEER TAG enables your users to manage all
the beers that they have drank and want to drink. Each beer has detailed information about it from
the ABV (alcohol by volume) to the style and description. Data is community driven and every beer
lover can add new beers. Also, BEER TAG allows you to rate a beer and see average rating from
different users. You can refer to https://untappd.com/ as a real world example.

## Functional Requirements
### General
Each user has name, email and profile picture.
Each beer has name, description, origin country, brewery that produces it, style (pre-defined), ABV,
zero, one or many tags (think Instagram hashtags) and a picture.
Application provides functionality based on the role of the user that is using it. There are three types
of users: anonymous, regular and administrator.
Anonymous users can browse all beers and their details. They can also filter by origin country, style,
tags and sort by name, ABV and rating.
Registered users can create new beers, add a beer to their wish list and drunk list and rate beers.
They can also edit and delete their own beers. Registered users can modify their personal
information as well.
Administrators can edit/delete all beers, users, breweries and styles.


**Note:** The authentication and role management mechanism will be studied last thus you can’t
implement it from the beginning. We will show you how to imitate authentication so that you can
implement your functionality.
REST API
In order to collaborate with the QA team or provide other developers with your service, you need to
develop a REST API. The REST API should leverage HTTP as a transport protocol and clear text
JSON for the request and response payloads.

API documentation is the information that is required to successfully consume and integrate with an
API. Use Swagger to document yours.
REST API Specification:
1. Breweries
    1. CRUD Operations
1. Styles
    1. CRUD Operations
1. Beers
    1. CRUD Operations
    1. List all beers
        * Filter by country, style, tags
        * Sort by name, ABV, rating
    1. Rate a beer
1. Users
    1. CRUD Operations
    1. Add beer to wish list
    1. Get wish list beers
    1. Add beer to drunk list
    1. Get drunk list beers

**Note:** All privacy rules apply for REST API endpoints. For example, anonymous user cannot
create/edit a beer or add it to his wish list.
## Front-end
### Public Part
The public part of your application should be accessible without authentication. This includes the
application start page, the user login and user registration forms, as well as list of all beers that have
been added in the system. People that are not authenticated cannot see any user specific details,
they can only browse the beers and see details of them.

### Private Part (regular users)
Registered users should have private part in the web application accessible after successful
login.
The web application provides them with UI to add/edit/delete (CRUD) beers. Each user can rate a
beer. Both user rating and average rating should be visible for the users. There should be masterdetails
view that will show beer details when one is selected. It is your choice of design where and
what properties to visualize, but all data should be visible somewhere.
The app should have user profile page that shows user’s photo, details and their top 3 most ranked
beers, they’ve tasted.

### Administration Part
System administrators should have administrative access to the system and permissions to
administer all entities e.g. to edit/delete (CRUD) users and other administrators or edit/delete beers
and related data if they decide to.
Note: UI for managing countries, breweries, styles and tags is NOT required.
Technical Requirements
General development guidelines include, but are not limited to:
* Use IntelliJ IDEA
* Following OOP principles when coding
* Following KISS, SOLID, DRY principles when coding
* Following REST API design best practices when designing the REST API (see Apendix)
* Following BDD when writing tests
* You should implement sensible Exception handling and propagation

### Database
The data of the application MUST be stored in a relational database – MySQL/MariaDB. You need
to identify the core domain objects and model their relationships accordingly.
Backend
* JDK version 11
* Use tiered project structure (separate the application components in layers)
* Use SpringMVC and SpringBoot framework
* Use Hibernate in the persistence (repository) layer
* Use Spring Security to handle user registration and user roles
* Service layer (i.e. “business” functionality) should have at least 80% unit test coverage

### Frontend
* Use Spring MVC Framework with Thymeleaf template engine for generating the UI
* Use AJAX for making asynchronous requests to the server where you find it appropriate
* You may change the standard theme and modify it to apply own web design and visual
styles. For example you could search and use some free html & css template to make your
web application look good.
* You may use Bootstrap or Materialize
Note: Please keep in mind that your web application should be build using Spring MVC Framework
with Thymeleaf template engine. The REST API is only for external usage of your services.

### Optional Requirements
* Integrate your project with a Continuous Integration server (e.g. GitLab, Jenkins or other).
Configure your unit tests to run on each commit to your master branch
* Host your application’s backend in a public hosting provider of your choice (e.g. AWS, Azure)
* Implement logging functionality for any issues/exceptions, should they arise (Logging in
Spring)
Deliverables
Provide link to a Git repository with the following information:
* Link to the Trello board
* Commits in the GitLab repository should give a good overview of how the project was
developed, which features were created first etc. and the people who contributed.
Contributions from all team members MUST be evident through the git commit history!
* Read https://dev.to/pavlosisaris/git-commits-an-effective-style-guide-2kkn and
https://chris.beams.io/posts/git-commit/ for a guide to write good commit messages
* The repository MUST contain the complete application source code and any run scripts
* The project MUST have at least README.md documentation describing how to build and
run the project
* Screenshots of the major application user facing screens with some data on them
* URL of the application (if hosted online)


Project Defense
Each team will have a public defense of their work to the trainers and students. It includes a
live demonstration of the developed web application (please prepare sample data). Also, each team
will have a defense of their project with the trainers where they must explain the application
structure, major architectural components and selected source code pieces demonstrating the
implementation of key features.

Expectations
You MUST understand the system you have created.
Any defects or incomplete functionality MUST be properly documented and secured.
It’s OK if your application has flaws or is missing one or two MUST’s. What’s not OK is if you don’t
know what’s working and what isn’t and if you present an incomplete project as functional.
Some things you need to be able to explain during your project defense:
* What are the most important things you’ve learned while working on this project?
* What are the worst “hacks” in the project, or where do you think it needs improvement?
* What would you do differently if you were implementing the system again?

Appendix
Guidelines for designing good REST API

https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy
Guidelines for URL encoding
http://www.talisman.org/~erlkonig/misc/lunatech%5Ewhat-every-webdev-must-know-about-urlencoding/
Always prefer constructor injection
https://www.vojtechruzicka.com/field-dependency-injection-considered-harmful/



