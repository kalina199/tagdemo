-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for data_beer_tag
CREATE DATABASE IF NOT EXISTS `data_beer_tag` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `data_beer_tag`;

-- Dumping structure for table data_beer_tag.beers
CREATE TABLE IF NOT EXISTS `beers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `origin_country_id` int(11) NOT NULL,
  `brewery_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  `ABV` float NOT NULL,
  `picture` blob NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `beers_breweries_id_fk` (`brewery_id`),
  KEY `beers_origin_countries_id_fk` (`origin_country_id`),
  KEY `beers_styles_id_fk` (`style_id`),
  KEY `beers_users_id_fk` (`user_id`),
  CONSTRAINT `beers_breweries_id_fk` FOREIGN KEY (`brewery_id`) REFERENCES `breweries` (`id`),
  CONSTRAINT `beers_origin_countries_id_fk` FOREIGN KEY (`origin_country_id`) REFERENCES `origin_countries` (`id`),
  CONSTRAINT `beers_styles_id_fk` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id`),
  CONSTRAINT `beers_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.beers: ~10 rows (approximately)
/*!40000 ALTER TABLE `beers` DISABLE KEYS */;
INSERT INTO `beers` (`id`, `name`, `description`, `origin_country_id`, `brewery_id`, `style_id`, `ABV`, `picture`, `user_id`, `is_deleted`) VALUES
	(1, 'staropramen', 'ok', 1, 3, 1, 4, _binary 0x737461726F7072616D656E2E6A7067, 1, 0),
	(2, 'kamenitza', 'ok', 2, 2, 2, 5, _binary 0x6B616D656E69747A612E6A7067, 2, 0),
	(3, 'zagorka', 'ok', 3, 1, 3, 4.5, _binary 0x7A61676F726B612E6A7067, 3, 0),
	(4, 'Astika', 'nice, a?', 1, 4, 1, 4, _binary 0x612E6A7067, 8, 0),
	(5, 'Carlsberg', 'flat', 1, 5, 4, 5, _binary 0x6B2E6A7067, 5, 0),
	(6, 'Tuborg', 'tasty', 1, 5, 2, 4.6, _binary 0x7475626F72672E6A7067, 4, 0),
	(7, 'Corona', 'flat', 5, 8, 7, 3, _binary 0x6B2E6A7067, 10, 0),
	(8, 'Busch', 'good', 6, 5, 8, 10, _binary 0x6B2E6A7067, 6, 0),
	(9, 'Amstel', 'ok', 8, 5, 7, 3, _binary 0x6B2E6A7067, 9, 0),
	(10, 'Haineken', 'super', 9, 5, 3, 8, _binary 0x6B2E6A7067, 7, 0);
/*!40000 ALTER TABLE `beers` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.beers_tags
CREATE TABLE IF NOT EXISTS `beers_tags` (
  `beer_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  KEY `beers_tags_beers_id_fk` (`beer_id`),
  KEY `beers_tags_tags_id_fk` (`tag_id`),
  CONSTRAINT `beers_tags_beers_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`),
  CONSTRAINT `beers_tags_tags_id_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.beers_tags: ~10 rows (approximately)
/*!40000 ALTER TABLE `beers_tags` DISABLE KEYS */;
INSERT INTO `beers_tags` (`beer_id`, `tag_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 10),
	(4, 3),
	(5, 4),
	(6, 6),
	(7, 7),
	(8, 9),
	(9, 8),
	(10, 5);
/*!40000 ALTER TABLE `beers_tags` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.breweries
CREATE TABLE IF NOT EXISTS `breweries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.breweries: ~11 rows (approximately)
/*!40000 ALTER TABLE `breweries` DISABLE KEYS */;
INSERT INTO `breweries` (`id`, `name`, `is_deleted`) VALUES
	(1, 'Zagorka', 0),
	(2, 'Shumensko', 0),
	(3, 'Haineken', 0),
	(4, 'Kamenitza', 0),
	(5, 'Karlsberg', 0),
	(6, 'Mythos', 0),
	(7, 'Amstel', 0),
	(8, 'Busch', 0),
	(9, 'Carlsberg', 0),
	(10, 'Corona', 0),
	(11, 'Hejneken', 0);
/*!40000 ALTER TABLE `breweries` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.drunk_lists
CREATE TABLE IF NOT EXISTS `drunk_lists` (
  `user_id` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  KEY `drunk_lists_beers_id_fk` (`beer_id`),
  KEY `drunk_lists_users_id_fk` (`user_id`),
  CONSTRAINT `drunk_lists_beers_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`),
  CONSTRAINT `drunk_lists_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.drunk_lists: ~10 rows (approximately)
/*!40000 ALTER TABLE `drunk_lists` DISABLE KEYS */;
INSERT INTO `drunk_lists` (`user_id`, `beer_id`) VALUES
	(1, 3),
	(2, 1),
	(3, 2),
	(4, 6),
	(5, 8),
	(6, 10),
	(7, 4),
	(8, 5),
	(9, 7),
	(10, 9);
/*!40000 ALTER TABLE `drunk_lists` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.origin_countries
CREATE TABLE IF NOT EXISTS `origin_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(20) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.origin_countries: ~11 rows (approximately)
/*!40000 ALTER TABLE `origin_countries` DISABLE KEYS */;
INSERT INTO `origin_countries` (`id`, `country`, `is_deleted`) VALUES
	(1, 'Bulgaria', 0),
	(2, 'Belgium', 0),
	(3, 'Germany', 0),
	(4, 'Greece', 0),
	(5, 'Czech Republic', 0),
	(6, 'USA', 0),
	(7, 'Mexico', 0),
	(8, 'Japan', 0),
	(9, 'France', 0),
	(10, 'Japan', 0),
	(11, 'Italy', 0);
/*!40000 ALTER TABLE `origin_countries` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.ratings
CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT NULL,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ratings_beers_id_fk` (`beer_id`),
  KEY `ratings_users_id_fk` (`user_id`),
  CONSTRAINT `ratings_beers_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`),
  CONSTRAINT `ratings_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.ratings: ~12 rows (approximately)
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` (`id`, `rating`, `beer_id`, `user_id`) VALUES
	(1, 2, 1, 1),
	(2, 3, 2, 2),
	(3, 5, 6, 5),
	(4, 4, 3, 10),
	(5, 4, 8, 3),
	(6, 2, 10, 8),
	(7, 1, 4, 7),
	(8, 2, 9, 4),
	(9, 4, 5, 6),
	(10, 5, 7, 9),
	(11, 3, 1, 3),
	(12, 5, 1, 5);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_roles_role_uindex` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role`) VALUES
	(3, 'administrator'),
	(2, 'regular');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.styles
CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `style` varchar(20) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.styles: ~10 rows (approximately)
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` (`id`, `style`, `is_deleted`) VALUES
	(1, 'Ale', 0),
	(2, 'Lager', 0),
	(3, 'Stout', 0),
	(4, 'Pilsner', 0),
	(5, 'Malt', 0),
	(6, 'Dark', 0),
	(7, 'Pale', 0),
	(8, 'Red', 0),
	(9, 'Lime', 0),
	(10, 'Fruit', 0);
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.tags: ~14 rows (approximately)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`id`, `tag`, `is_deleted`) VALUES
	(1, '#hubavabira', 0),
	(2, '#birata', 0),
	(3, '#superbira', 0),
	(4, '#chocolate', 0),
	(5, '#blonde', 0),
	(6, '#ale', 0),
	(7, '#lager', 0),
	(8, '#bitter', 0),
	(9, '#hoppy', 0),
	(10, '#spicy', 0),
	(11, '#mild', 0),
	(12, '#pilsner', 0),
	(13, '#Dutch', 0),
	(14, '#seasonal', 0);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `profile_picture` blob DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.users: ~16 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `profile_picture`, `is_deleted`) VALUES
	(1, 'Pesho', 'pesho@telerik.com', _binary 0x706573686F2E6A7067, 0),
	(2, 'Gosho', 'gosho@telerik.com', _binary 0x676F73686F2E6A7067, 0),
	(3, 'Tosho', 'tosho@telerik.com', _binary 0x746F73686F2E6A7067, 0),
	(4, 'Milena', 'milena@yahoo.com', _binary 0x6D696C656E612E6A7067, 0),
	(5, 'Ivan', 'ivan@abv.bg', _binary 0x6976616E2E6A7067, 0),
	(6, 'Stamat', 'StanislavMatematik@a.bg', _binary 0x7374616D61742E6A7067, 0),
	(7, 'Petar', 'peshoMat@yahoo.com', _binary 0x70657461722E6A7067, 0),
	(8, 'Drago', 'drago@abv.bg', _binary 0x647261676F2E6A7067, 0),
	(9, 'Plamen', 'plamen@abv.bg', _binary 0x706C616D656E2E6A7067, 0),
	(10, 'Ina', 'ina@abv.bg', _binary 0x696E612E6A7067, 0),
	(11, 'Test', 'test@telerik.com', NULL, 0),
	(12, 'new2', 'test@telerik.com', NULL, 0),
	(13, 'Test3', 'test@telerik.com', NULL, 0),
	(14, 'user', 'test@telerik.com', NULL, 0),
	(15, 'user', 'test@telerik.com', NULL, 0),
	(16, 'userR', 'tes2t@telerik.com', NULL, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
  `users_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  KEY `users_roles_roles_id_fk` (`roles_id`),
  KEY `users_roles_users_id_fk` (`users_id`),
  CONSTRAINT `users_roles_roles_id_fk` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_roles_users_id_fk` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.users_roles: ~12 rows (approximately)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`users_id`, `roles_id`) VALUES
	(1, 2),
	(2, 2),
	(3, 2),
	(4, 2),
	(5, 2),
	(6, 2),
	(7, 3),
	(7, 2),
	(8, 3),
	(8, 2),
	(9, 2),
	(10, 2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

-- Dumping structure for table data_beer_tag.wish_lists
CREATE TABLE IF NOT EXISTS `wish_lists` (
  `user_id` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  KEY `wish_lists_beers_id_fk` (`beer_id`),
  KEY `wish_lists_users_id_fk` (`user_id`),
  CONSTRAINT `wish_lists_beers_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`id`),
  CONSTRAINT `wish_lists_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table data_beer_tag.wish_lists: ~4 rows (approximately)
/*!40000 ALTER TABLE `wish_lists` DISABLE KEYS */;
INSERT INTO `wish_lists` (`user_id`, `beer_id`) VALUES
	(1, 1),
	(1, 2),
	(2, 1),
	(3, 2);
/*!40000 ALTER TABLE `wish_lists` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
